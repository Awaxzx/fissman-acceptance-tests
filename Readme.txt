Это свежая версия Codeception и она отличается от того, что сейчас идет вместе с сайтом.
Поскольку тут только приемочные тесты, то систему можно развернуть на любом сервере, т.к. код сайта и база им не нужна.
Если в дальнейшем будут использоваться функциональные тесты, то тогда нужно будет обновить codeception идущий с сайтом и соответственно тесты скопировать туда.

Сейчас реализовано тестирование только главной страницы. Сделать тестирование статических страниц много времени не займет.
Но как я понимаю, в основном необходимо тестировать каталог, корзину, процесс покупки и личный кабинет.

Кстати пока тестировал нашел баг в выборе городов: если заполнить текстовое поле с названием города и кликнуть кнопку "Выбрать", то попадаешь на страницу с ошибкой.

Установка:

1. Сделать composer install
2. Запустить selenium-сервер. На всякий случай положил его в корень, но можно установить и через composer глобально.
3. Запустить chromedriver.exe Тоже лежит тут же в корне.
4. Собственно запустить тесты: codecept run --steps --html

После выполнения теста, в папке /tests/_output будет сгенерирован файл report.html Его можно открыть в браузере и посмотреть что было выполнено. Там на "+" кликаешь и разворачиваются списки с проведенными проверками.

Основные настройки в файле /tests/acceptance.suite.yml Там прописывается адрес тестируемого сайта.
Сейчас настроен на тестирования рабочего сайта. В дальнейшем нужно тестировать на тестовом сервере, т.к. нужно будет проверять и заказы и регистрацию пользователей.
