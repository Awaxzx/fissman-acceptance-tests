<?php
/**
 * Created by PhpStorm.
 * User: Awax
 * Date: 31.05.2020
 * Time: 13:36
 */

namespace Page;

class Elements
{
    public static function testHeader(\AcceptanceTester $I)
    {
        /**
         * Текст в логотипе
         */
        $text = str_replace(["\r", "\n"], ' ', strip_tags($I->grabTextFrom("//span[@class='slogan']")));
        $I->assertEquals('Официальный сайт продукции Fissman в России', $text);

        // Проверяем выбор города
        self::testCitySelector($I);

        // Наличие телефона в шапке
        $I->seeElement("//a[@class='header_phone']");
        $I->assertNotEmpty($I->grabTextFrom("//a[@class='header_phone']"));

        // Верхнее мини-меню
        $I->see("Избранное", "//div[@class='header__meta']//div[1]/a");
        $I->see("Корзина", "//div[@class='header__meta']//div[2]/a");
        $I->see("Войти в кабинет", "//div[@class='header__meta']//div[3]/a");

        // Проверяем кнопку каталога
        self::testCatalogButton($I);
        $I->see("Шок-цена!");

        // Поле поиска
        self::testSearchField($I);

        $I->see("О компании");
        $I->see("Оплата и доставка");
        $I->see("Покупателям");
        $I->see("Сотрудничество");

    }

    /** Проверяем выбор города
     * @param \AcceptanceTester $I
     * @throws \Exception
     */
    public static function testCitySelector(\AcceptanceTester $I)
    {
        $I->see("Ваш город");

        // Видим Москва в качестве выбранного города
        $I->see("Москва", "//a[@class='header_choose_city open_modal']");

        //Кликаем на название города
        $I->click("//a[@class='header_choose_city open_modal']");

        // Видим меню выбора городов
        $I->waitForElementVisible('#cities_modal', 60);
        $I->see("Выберите город");

        // Закрываем меню
        $I->click("//div[@id='cities_modal']/div/a[@class='modal_close']");
        // Не видим меню выбора городов
        $I->wait(5);
        $I->dontSeeElement("#cities_modal");

        // Открываем меню снова
        $I->click("//a[@class='header_choose_city open_modal']");
        $I->waitForElementVisible('#cities_modal', 60);

        // Видим в меню город Пятигорск
        $I->see("Пятигорск", "//div[@class='ip_city']/label");

        // Кликаем на него (2-й в списке)
        $I->click('//div[@class=\'ip_city\'][2]/label');

        $I->wait(5);

        // Не видим меню выбора городов
        $I->dontSeeElement("#cities_modal");
        // Видим Пятигорск в качестве выбранного города
        $I->see("Пятигорск", "//a[@class='header_choose_city open_modal']");

        // Кликаем на Пятигорск
        $I->click("//a[@class='header_choose_city open_modal']");
        // Видим меню выбора городов
        $I->waitForElementVisible('#cities_modal', 60);
        // Вводим в поле выбора название города
        $I->fillField("//input[@class='input_text another_city']", 'Ессентуки');
        // Видим выпадающее меню с подсказкой
        $I->waitForElementVisible('.cities_dropdown', 120);
        // В подсказке видим текст Ессентуки, Ставропольский край
        $I->wait(5);
        $I->see("Ессентуки, Ставропольский край", "//a[@class='city_dropdown']");
        // Кликаем на него
        $I->click("//a[@class='city_dropdown']");

        $I->wait(5);

        // Не видим меню выбора городов
        $I->dontSeeElement("#cities_modal");
        // Видим Ессентуки в качестве выбранного города
        $I->see("Ессентуки", "//a[@class='header_choose_city open_modal']");
    }

    /** Проверяем кнопку каталога и выпадающее меню
     * @param \AcceptanceTester $I
     * @throws \Exception
     */
    public static function testCatalogButton(\AcceptanceTester $I)
    {
        // Открываем меню
        // Видим кнопку Каталог
        $I->see("Каталог", "//div[@class='topnav_buttons']/a/span[@class='relative']");
        // Кликаем на нее
        $I->click("//div[@class='topnav_buttons']/a[@class='button blue catalog_open']");
        // Ожидаем увидеть открывшееся меню
        $I->waitForElementVisible("//nav[@class='catalog_menu_box opened']", 5);
        // Наводим мышь на первый пункт меню
        $I->moveMouseOver("//ul[@class='catalog_menu']/li[1]/a");
        // Ожидаем увидеть открывшееся подменю для первого пункта
        $I->waitForElementVisible("//ul[@class='catalog_menu']/li[1]/ul[@class='catalog_submenu']", 5);
        // Наводим мышь на второй пункт меню
        $I->moveMouseOver("//ul[@class='catalog_menu']/li[2]/a");
        // Перестаем видеть подменю для первого пункта
        $I->waitForElementNotVisible("//ul[@class='catalog_menu']/li[1]/ul[@class='catalog_submenu']", 5);
        // Ожидаем увидеть подменю для второго пункта
        $I->waitForElementVisible("//ul[@class='catalog_menu']/li[2]/ul[@class='catalog_submenu']", 5);

        // Закрываем меню
        // Кликаем на кнопку Каталог
        $I->click("//div[@class='topnav_buttons']/a[@class='button blue catalog_open opened']");
        // Перестаем видеть открытое меню
        $I->waitForElementNotVisible("//nav[@class='catalog_menu_box opened']", 5);
    }

    /** Проверяем поле поиска
     * @param \AcceptanceTester $I
     * @throws \Exception
     */
    public static function testSearchField(\AcceptanceTester $I, $request = "Сковорода")
    {
        // Видим кнопку поиск
        $I->see("Поиск", "//button[@class='topnav_search_button']");

        // Вводим в поле поиска запрос $request
        $I->fillField("//input[@class='topnav_search_input']", $request);
        $I->wait(5);

        $I->waitForElementVisible("//div[@id='mCSB_1_container']", 5);
        $I->wait(5);
        $results = $I->grabMultiple("//div[@id='mCSB_1_container']/div//div[@class='search_dropdown_product']/div[@class='sdp_info_box']/a", 'innerHTML');
        $I->assertGreaterThan(0, count($results), count($results). " результатов поиска по запросу \"$request\".");

        // Не удается проскролить слой до данной кнопки чтобы она стала видимой
        //$I->see("Показать все", "//div[@id='mCSB_1_container']/a[@class='button search_see_all']");

        // Вводим в поле поиска запрос по которому не должно быть результатов
        $I->fillField("//input[@class='topnav_search_input']", "qwerty12354456");
        $I->wait(5);
        $I->see("Товары не найдены", "//div[@id='mCSB_1_container']/div/div[@class='search-not_fount__container']");

    }

    /** Проверяем футер
     * @param \AcceptanceTester $I
     */
    public static function testFooter(\AcceptanceTester $I)
    {
        $I->see('В начало страницы');
        $I->see("Официальный сайт продукции Fissman в России", "//span[@class='fslogan']");

        $I->see('Мы принимаем к оплате'); // Пропадает в мобильной версии сайта

        // Меню каталога и количество ссылок
        $I->see("Каталог", "//ul[@class='fmenu']/li[@class='fmenu_first']/a");
        $items = $I->grabMultiple("//div[@class='fcol'][1]/ul[@class='fmenu'][1]//li[position()>1]/a", 'innerHTML');
        $I->assertGreaterThan(0, count($items), count($items). ' пунктов в нижнем меню каталога');

        // Меню страниц и количество ссылок
        $I->see("Личный кабинет", "//ul[@class='fmenu']/li[@class='fmenu_first']/a");
        $items = $I->grabMultiple("//div[@class='fcol'][2]/ul[@class='fmenu'][1]//li[position()>1]/a", 'innerHTML');
        $I->assertGreaterThan(0, count($items), count($items). ' пунктов в нижнем меню страниц');

        // Телефон
        $I->seeElement("//a[@class='fphone']");
        $I->assertNotEmpty($I->grabTextFrom("//a[@class='fphone']"));

        // Режим работы и количество записей
        $I->see('Режим работы');
        $items = $I->grabMultiple("//div[@class='worktime']/ul//li", 'innerHTML');
        $I->assertGreaterThan(0, count($items), count($items). ' пунктов в списке "Часы работы"');

        $I->see('© Интернет-магазин посуды Fissman, '.date('Y'));
        $I->see('Пользовательское соглашение');
        $I->see('Политика конфиденциальности');
        $I->see('Разработка сайта — WEBELEMENT');
    }
}