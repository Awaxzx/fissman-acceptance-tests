<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
 */
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    /**
     * Define custom actions here
     */


    /**
     * Если тестируемый сайт запускается на сервере без сертификата, в браузере отображается предупреждение.
     * Обойти его можно кликнув по соответствующим ссылкам на странице этого предупреждения.
     * Я нашел как в настройках WebDriver-а заставить браузер (Chrome) игнорировать ошибку сертификата.
     * Так что данный метод можно не использовать.
     */
    public function skipSSLError()
    {
        $this->click('button#details-button');
        $this->click('a#proceed-link');
    }
}
