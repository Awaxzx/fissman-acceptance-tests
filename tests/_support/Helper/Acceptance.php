<?php

namespace Helper;


// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Acceptance extends \Codeception\Module
{
    /** Определяет текущий URL, переходит по заданному URL, выполняет проверки заданные в кложуре и возвращается на
     * исходный URL. Статус запроса через WebDriver проверить нельзя
     * @param $url
     * @param \AcceptanceTester $I
     * @param \Closure $closure
     * @throws \Codeception\Exception\ModuleException
     */
    public function checkUrlAndReturnBack($url, \AcceptanceTester $I, \Closure $closure)
    {
        $currentUrl = $this->getModule('WebDriver')->_getCurrentUri();
        $url        = parse_url($url, PHP_URL_PATH);
        $I->amOnPage($url);
        $closure();
        $I->amOnPage($currentUrl);
    }
}
