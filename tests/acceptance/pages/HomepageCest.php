<?php

namespace tests\acceptance\pages;

use AcceptanceTester;
use \Page\Elements;

class HomepageCest
{
    /** Проверка главной страницы
     * @param AcceptanceTester $I
     */
    public function tryToTestHomepage(AcceptanceTester $I)
    {
        $I->wantToTest('Главную страницу сайта Fissman');

        $I->amOnPage('/');
        $I->wait(2);

        Elements::testHeader($I);

        $this->_testSlider($I);
        $this->_testAdvantages($I);
        $this->_testShockPriceBlock($I);
        $this->_testBestSellers($I);
        $this->_testCategoriesBlocks($I);

        /**
         * Seo блок
         */
        $I->see('Посуда FISSMAN');

        Elements::testFooter($I);

    }

    /**
     * Есть слайдер и количество кадров больше чем 0
     */
    private function _testSlider(AcceptanceTester $I, $checkLinks = false)
    {
         $I->seeElement('.homeslider');
        $sliderItems = $I->grabMultiple("//div[@class='slider_infobox']/div", 'innerHTML');
        $I->assertGreaterThan(0, count(array_unique($sliderItems)), 'Количество кадров слайдера больше 0');

        // Проверка ссылкок слайдера. Надо ли? По-умолчанию выключено.
        if ($checkLinks)
        {
            $sliderLinks = $I->grabMultiple("//div[@class='slider_infobox']/a", 'href');
            $sliderLinks = array_unique($sliderLinks);

            foreach ($sliderLinks as $link)
            {
                $I->checkUrlAndReturnBack($link, $I, function () use ($I) {

                    $I->dontSee('Not Found (#404)');
                });
            }
        }
    }


    /**
     * Блок преимуществ
     */
    private function _testAdvantages(AcceptanceTester $I)
    {
        $I->seeElement('.prs_box.main_block');
        $prsItems = $I->grabMultiple("//div[@class='prs_box main_block']/div", 'innerHTML');
        $I->assertEquals(4, count($prsItems), '4 преимущества');
    }

    /**
     * Блок Успейте купить по шок-цене!
     */
    private function _testShockPriceBlock(AcceptanceTester $I)
    {
        $I->see('Успейте купить по шок-цене!');
        $I->seeElement('.products_carousel_box');
        $productsSliderItems = $I->grabMultiple("//div[@class='products_carousel_box'][1]//a[@class='product_item-title']", 'innerHTML');
        $I->assertNotEmpty($productsSliderItems);

        $I->see("Смотреть все товары со скидкой");
    }

    /**
     * Лидеры продаж
     */
    private function _testBestSellers(AcceptanceTester $I)
    {
        $I->see('Лидеры продаж');
        $I->seeElement('.products_carousel_box');
        $productsSliderItems = $I->grabMultiple("//div[@class='products_carousel_box'][2]//a[@class='product_item-title']", 'innerHTML');
        $I->assertNotEmpty($productsSliderItems);
    }

    /**
     * Блоки категорий
     */
    private function _testCategoriesBlocks(AcceptanceTester $I)
    {
        $I->see("Готовьте с удовольствием");
        $I->see("Устройте незабываемое чаепитие с ароматной домашней выпечкой");
        $I->see("Позаботьтесь о красивой подаче ваших кулинарных шедевров");
        $categoryItems = $I->grabMultiple("//div[@class='home_catalog_box']/nav[@class='home_catalog_item']/ul[@class='home_catalog_item-menu']/li/a", 'innerHTML');
        $categoryItems = array_map(function ($item) {
            return strip_tags($item);
        }, $categoryItems);
        $I->assertNotEmpty($categoryItems);
        $I->assertGreaterThan(0, count($categoryItems), 'В блоках категорий кол-во ссылок:' . count($categoryItems));
    }
}
